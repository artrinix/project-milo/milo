import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    kotlin("kapt")
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

group = "xyz.artrinix"
version = System.getenv("MILO_VERSION") ?: "Unknown"

repositories {
    mavenCentral()
    maven { url = uri("https://jitpack.io") }
    maven { url = uri("https://m2.dv8tion.net/releases") }
    maven { url = uri("https://oss.sonatype.org/content/repositories/snapshots") }
    maven { url = uri("https://schlaubi.jfrog.io/artifactory/lavakord") }
}

dependencies {
    /// Core dependencies
    // Kotlin dependencies
    listOf("stdlib-jdk8", "reflect").forEach { implementation(kotlin(it)) }
    // Kotlin Serialization
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2")
    implementation("com.charleskorn.kaml:kaml:0.38.0")
    // Kotlin Coroutines
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0-native-mt")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-debug:1.6.0")
    // Kotlin Scripting, used within the eval processor
    runtimeOnly(kotlin("scripting-jsr223"))
    // HTTP Client
    val ktorVersion: String by project
    implementation("io.ktor:ktor-client-core:$ktorVersion")
    implementation("io.ktor:ktor-client-cio:$ktorVersion")
    implementation("io.ktor:ktor-client-serialization:$ktorVersion")
    implementation("io.ktor:ktor-client-logging:$ktorVersion")

    /// Logger framework
    api("org.slf4j:slf4j-api:1.7.32")
    implementation("ch.qos.logback:logback-classic:1.2.10")
    implementation("ch.qos.logback:logback-core:1.2.10")
    implementation("io.github.microutils:kotlin-logging-jvm:2.1.21")

    /// Discord API stuff
    // JDA - The main library used to connect to Discord, minus the audio module
    implementation("net.dv8tion:JDA:5.0.0-alpha.3") { exclude(module = "opus-java") }
    // MinnDevelopment's Kotlin extensions for JDA - A nice to have for development and reduces boilerplate code
    implementation("com.github.minndevelopment:jda-ktx:d3c6b4d")
    // Lavalink-Client - This is how we play music, might get replaced with Artrinix Starlink when we start working on that
    //implementation("com.github.freyacodes:Lavalink-Client:8d9b660") { exclude(module = "lavaplayer") }
    implementation("dev.schlaubi.lavakord:jda:3.1.0")
    implementation("com.sedmelluq:lavaplayer:1.3.77")
    // Aviation - Our in-house command and event framework sort of. This is how we handle commands
    // Due to the constant changes and hard requirement in the code, we bundle Aviation as part of the build process
    // This allows us to make critical changes whilst working on Milo directly
    implementation(project(":aviation"))

    /// Database Clients
    // PostgresSQL
    val exposedVersion: String by project
    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("org.postgresql:postgresql:42.3.1")
    // Redis
    implementation("io.lettuce:lettuce-core:6.1.5.RELEASE")
    // InfluxDB
    implementation("com.influxdb:influxdb-client-kotlin:4.0.0")
    implementation("com.influxdb:flux-dsl:4.0.0")
}

tasks.withType<KotlinCompile>() {
    kotlinOptions {
        jvmTarget = "11"
        freeCompilerArgs = listOf("-Xinline-classes", "-Xopt-in=kotlin.RequiresOptIn")
    }
}

tasks.withType<ShadowJar>() {
    archiveFileName.set("milo.jar")
    manifest {
        attributes(
            mapOf(
                "Main-class" to "xyz.artrinix.milo.Milo",
                "Implementation-Version" to archiveVersion.get()
            )
        )
    }
}