package xyz.artrinix.milo.commands.economy

import dev.minn.jda.ktx.Embed
import dev.minn.jda.ktx.await
import net.dv8tion.jda.api.entities.User
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.common.database.tables.guild.GuildUser
import xyz.artrinix.milo.common.database.tables.guild.leveling.Leveling
import xyz.artrinix.milo.common.modules.leveling.LevelingModule
import xyz.artrinix.milo.utils.newSuspendedTransaction
import java.awt.Color

class RankCommand : AviationCog {
    @Command(aliases = ["level", "r"])
    suspend fun rank(ctx: AviationContext, user: User?) {
        val levelConfig = newSuspendedTransaction { Leveling.findById(ctx.guild.idLong) }

        if (levelConfig == null || !levelConfig.enabled) {
            ctx.reply("Leveling is disabled here.")
            return
        }

        val profile = newSuspendedTransaction { GuildUser.findById(user?.idLong ?: ctx.author.idLong) }

        if (profile == null) {
            if (user == null) ctx.reply("You don't have any experience yet, try again after sending a message")
            else ctx.channel
                .sendMessage("${user.asMention} doesn't have any experience yet.")
                .reference(ctx.message)
                .allowedMentions(
                    listOf()
                )
                .await()
            return
        }

        val leaderboard = LevelingModule.leaderboard()

        ctx.channel.sendMessageEmbeds(
            Embed {
                title = "${user?.asTag ?: ctx.author.asTag} | Level ${profile.level}"
                description = "**${profile.xp}** / **${LevelingModule.requiredXpForLevel(profile.level + 1)}** until Level ${profile.level + 1}"
                color = Color.decode("#F2CDCD").rgb
            }
        ).reference(ctx.message).await()
    }
}