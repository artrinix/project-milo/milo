package xyz.artrinix.milo.commands.music

import dev.schlaubi.lavakord.audio.Link
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.Milo

class VolumeCommand : AviationCog {
    @Command
    suspend fun volume(ctx: AviationContext, volume: Int?) {
        val link = Milo.lavalink.getLink(ctx.guild.id)
        if (link.state == Link.State.NOT_CONNECTED) {
            ctx.send("Not playing anything.")
            return
        }

        if (volume == null) {
            ctx.send("Current volume is ${link.player.volume}%")
        } else {
            link.player.setVolume(volume)
            ctx.send("Set volume to $volume%")
        }
    }
}