package xyz.artrinix.milo.commands.music

import dev.schlaubi.lavakord.audio.Link
import dev.schlaubi.lavakord.audio.player.*
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.annotations.SubCommand
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.Milo

class EqualizerCommand : AviationCog {
    @Command(
        aliases = ["eq"]
    )
    suspend fun equalizer(ctx: AviationContext) {
        ctx.send("Used for setting the equalizer, check help for options")
    }

    @SubCommand
    suspend fun band(ctx: AviationContext, band: Int, gain: Double) {
        val link = link(ctx) ?: return

        if (band < 1 || band > 15) {
            ctx.send("Invalid band, please pick one between 1 and 15.")
            return
        }

        if (gain < -0.25F || gain > 0.25F) {
            ctx.send("Invalid band, keep it between -0.25 - 0.25")
            return
        }

        link.player.applyEqualizer {
            band(band) gain(gain.toFloat())
        }
        ctx.send("Applied changes!")
    }

    @OptIn(FiltersApi::class)
    @SubCommand
    suspend fun speed(ctx: AviationContext, speed: Double = 1.0, pitch: Double = 1.0) {
        val link = link(ctx) ?: return

        link.player.applyFilters {
            timescale {
                this.speed = speed.toFloat()
                this.pitch = pitch.toFloat()
            }
        }
        ctx.send("Applied changes!")
    }

    @OptIn(FiltersApi::class)
    @SubCommand
    suspend fun karaoke(ctx: AviationContext, enable: Boolean = true) {
        val link = link(ctx) ?: return

        link.player.applyFilters {
            karaoke {
                level = if (enable) 5F else 0F
            }
        }

        ctx.send("Karaoke time!")
    }

    private suspend fun link(ctx: AviationContext): Link? {
        val link = Milo.lavalink.getLink(ctx.guild.id)
        return if (link.state == Link.State.NOT_CONNECTED) {
            ctx.send("I am not connected to a channel.")
            null
        } else {
            link
        }
    }
}