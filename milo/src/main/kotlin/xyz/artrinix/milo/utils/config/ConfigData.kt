package xyz.artrinix.milo.utils.config

import kotlinx.serialization.Serializable

@Serializable
data class ConfigData(
    val milo: MiloData = MiloData(),
    val services: ServicesData = ServicesData(),
    val database: DatabaseData = DatabaseData()
)

@Serializable
data class MiloData(
    val token: String = "",
    val id: Long = 0L,
    val prefix: List<String> = listOf(),
    val activity: String = "",
    val apiPort: Int = 8045,
    val developers: List<Long> = listOf(),
    val sharding: MiloSharding = MiloSharding(),
    val links: MiloLinks = MiloLinks()
)

@Serializable
data class MiloSharding(
    val homeGuildId: Long = 0L,
    var start: Int = 1,
    var end: Int = -1,
    var total: Int = -1,
    val bucketFactor: Int = 8
)

@Serializable
data class MiloLinks(
    val support: String = "https://artrinix.xyz/support",
    val website: String = "https://milo.artrinix.xyz",
    val invite: String = ""
)

@Serializable
data class ServicesData(
    val lavalinkNodes: List<LavalinkNode> = listOf(),
    val api: APIServicesData = APIServicesData()
)

@Serializable
data class LavalinkNode(
    val name: String = "",
    val host: String = "",
    val password: String = ""
)

@Serializable
data class APIServicesData(
    val twitch: String = "",
    val twitchSecret: String = ""
)

@Serializable
data class DatabaseData(
    val redis: RedisDatabase = RedisDatabase(),
    val postgres: PostgresDatabase = PostgresDatabase(),
    val influx: InfluxDatabase = InfluxDatabase()
)

@Serializable
data class RedisDatabase(
    val host: String = "",
    val port: Int = 0,
    val password: String = "",
    val prefix: String = ""
)

@Serializable
data class PostgresDatabase(
    val host: String = "",
    val port: Int = 0,
    val user: String = "",
    val password: String = "",
    val database: String = ""
)

@Serializable
data class InfluxDatabase(
    val host: String = "",
    val port: Int = 0,
    val bucket: String = "",
    val project: String = "",
    val token: String = ""
)