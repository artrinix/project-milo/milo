package xyz.artrinix.milo

import dev.minn.jda.ktx.Embed
import dev.minn.jda.ktx.await
import dev.minn.jda.ktx.injectKTX
import dev.minn.jda.ktx.listener
import dev.schlaubi.lavakord.LavaKord
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.events.DisconnectEvent
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.events.ReconnectedEvent
import net.dv8tion.jda.api.events.ResumedEvent
import net.dv8tion.jda.api.events.guild.GuildJoinEvent
import net.dv8tion.jda.api.sharding.ShardManager
import xyz.artrinix.aviation.AviationClient
import xyz.artrinix.milo.common.bot.AviationEventListener
import xyz.artrinix.milo.common.bot.MiloFramework
import xyz.artrinix.milo.common.database.DatabaseFramework
import xyz.artrinix.milo.common.language.LanguageFramework
import xyz.artrinix.milo.common.modules.autorole.AutoRoleModule
import xyz.artrinix.milo.common.modules.leveling.LevelingModule
import xyz.artrinix.milo.common.modules.selectionRoles.SelectionRolesModule
import xyz.artrinix.milo.utils.config.ConfigFramework
import xyz.artrinix.milo.utils.config.ConfigFramework.Companion.config
import java.time.OffsetDateTime
import java.util.concurrent.Executors
import kotlin.system.exitProcess

object Milo {
    private val logger = KotlinLogging.logger {}
    // contains the version packed with the application or defaults to DEV
    private val version = this::class.java.`package`.implementationVersion ?: "DEV"

    lateinit var database: DatabaseFramework
    lateinit var aviation: AviationClient
    lateinit var lavalink: LavaKord

    @JvmStatic
    fun main(args: Array<String>): Unit = runBlocking {
        Thread.currentThread().name = "Milo-Core-CoroutineScope"
        logger.info { "Starting Milo v${version}" }

        logger.info { "Loading config..." }
        ConfigFramework().loadConfig()

        logger.info { "Success! Loading database..." }
        database = DatabaseFramework
        initiateDatabase()

        logger.info { "Success! Loading languages..." }
        val languages = LanguageFramework().loadLanguages()

        logger.info { "Success! Loading MiloFramework(ShardBuilder)..." }
        MiloFramework.createMilo {
            injectKTX()
        }

        logger.info { "Assuming LavaKord has been injected, mapping it..."}
        lavalink = MiloFramework.instance.lavakord

        logger.info { "Success! Loading events..." }
        loadEvents(MiloFramework.instance.shardManager)

        logger.info { "Success! Adding Lavalink nodes..." }
        config.services.lavalinkNodes.forEach {
            MiloFramework.instance.lavakord.addNode(it.host, it.password, it.name)
        }

        logger.info { "Success! Creating Aviation client..." }
        aviation = AviationClient(
            shardManager = MiloFramework.instance.shardManager,
            defaultLanguage = languages.first(),
            prefixes = config.milo.prefix.toMutableList(),
            commandExecutor = Executors.newCachedThreadPool {
                return@newCachedThreadPool Thread().apply {
                    name = "Aviation-Command-Thread"
                }
            },
            ownerIds = config.milo.developers.toMutableSet()
        ).apply {
            registerDefaultParsers()
            commands.register("xyz.artrinix.milo.commands")
            addLanguage(*LanguageFramework.languages.toTypedArray())
            addEventListeners(AviationEventListener())
        }

        logger.info { " Loading Modules" }
        loadModules()

        logger.info { "Success! Milo is ready for action!" }
    }

    private fun initiateDatabase() {
        try {
            database.connect()
        } catch (err: Error) {
            logger.error(err) { "Failed to connect to the database, this is a crucial part of Milo so the process will now exit." }
            exitProcess(1)
        }
    }

    private fun loadModules() {
        LevelingModule(MiloFramework.instance.shardManager)
        AutoRoleModule(MiloFramework.instance.shardManager)
        SelectionRolesModule(MiloFramework.instance.shardManager)
    }

    private fun loadEvents(shardManager: ShardManager) {
        // START Connection Events
        // @TODO Add to Influx
        shardManager.listener<ReadyEvent> {
            logger.info { "JDA Shard ${it.jda.shardInfo.shardString} :: Ready." }
        }

        shardManager.listener<ResumedEvent> {
            logger.info { "JDA Shard ${it.jda.shardInfo.shardString} :: Resumed." }
        }

        shardManager.listener<ReconnectedEvent> {
            logger.info { "JDA Shard ${it.jda.shardInfo.shardString} :: Reconnected." }
        }

        shardManager.listener<DisconnectEvent> {
            logger.warn { "JDA Shard ${it.jda.shardInfo.shardString} :: Disconnected${if (it.isClosedByServer) " (closed by server)" else ""}. Code: ${it.serviceCloseFrame?.closeCode ?: -1} : ${it.closeCode}, Reason: ${it.clientCloseFrame?.closeReason ?: "Unknown"}" }
        }
        // END Connection Events

        shardManager.listener<GuildJoinEvent> { event ->
            // Don't run if the SelfMember joined a longish time ago. This avoids discord causing issues.
            if (event.guild.selfMember.timeJoined.isBefore(OffsetDateTime.now().minusSeconds(30))) return@listener

            logger.info { "JDA Shard ${event.jda.shardInfo.shardString} :: Joined Guild (ID: ${event.guild.id})" }
            // @TODO Add to Influx

            val channel = event.guild.textChannels.firstOrNull { it.canTalk() } ?: return@listener
            if (event.guild.selfMember.hasPermission(channel, Permission.MESSAGE_EMBED_LINKS)) {
                channel.sendMessageEmbeds(Embed {
                    title = "Milo just joined the party, where's the chips and dip?"
                    description = """
                        > Milo is a swiss army knife bot that is designed to serve you in just about any way possible.

                        > To get started, run **`@Milo setup`** and Milo will walk you thought the guided setup
                    """.trimIndent()
                    field {
                        name = "Need help setting up Milo or have a question? come ask away in our home/support server"
                        value = "[Artrinix Support](${config.milo.links.support})"
                    }
                    field {
                        name = "Think Milo is pretty cool and wanna add him to your own server?"
                        value = "[Milo Website](${config.milo.links.website})"
                    }

                    footer {
                        name = "Made by [Artrinix](https://artrinix.xyz) with <3"
                        iconUrl = event.jda.shardManager!!.getGuildById(config.milo.sharding.homeGuildId)?.iconUrl
                    }
                }).await()
            } else {
                channel.sendMessage("""
                    > **Milo just joined the party, where's the chips and dip?**

                    > Milo is a swiss army knife bot that is designed to serve you in just about any way possible.
                    > To get started, run **`@Milo setup`** and Milo will walk you thought the guided setup

                    > Need help setting up Milo or have a question? come ask away in our home/support server <${config.milo.links.support}>

                    > Think Milo is pretty cool and wanna add him to your own server? <${config.milo.links.website}>
                    > **Made by Artrinix with <3**
                """.trimIndent()).await()
            }
        }
    }
}