package xyz.artrinix.milo.common.database.tables.guild.autoRole

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.LongColumnType
import xyz.artrinix.milo.common.database.exposed.array

object AutoRoleConfig : LongIdTable("auto_role") {
    val enabled = bool("enabled").default(false)
    val giveOnlyAfterMessageWasSent = bool("give_only_after_message_was_sent").default(false)
    val roles = array<Long>("roles", LongColumnType()).default(arrayOf())
    val giveRolesAfter = long("give_roles_after").default(0)
}

class AutoRole(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<AutoRole>(AutoRoleConfig)

    var enabled by AutoRoleConfig.enabled
    var giveOnlyAfterMessageWasSent by AutoRoleConfig.giveOnlyAfterMessageWasSent
    var roles by AutoRoleConfig.roles
    var giveRolesAfter by AutoRoleConfig.giveRolesAfter
}