package xyz.artrinix.milo.common.bot

import dev.minn.jda.ktx.Embed
import dev.minn.jda.ktx.SLF4J
import dev.minn.jda.ktx.listener
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.interactions.components.ActionRow
import net.dv8tion.jda.api.interactions.components.Button
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.aviation.command.entities.CommandFunction
import xyz.artrinix.aviation.command.exceptions.BadArgument
import xyz.artrinix.aviation.command.hook.CommandEventAdapter
import xyz.artrinix.milo.Milo
import java.time.Instant
import java.util.concurrent.TimeUnit

class AviationEventListener : CommandEventAdapter {
    private val log by SLF4J
    override fun onBadArgument(ctx: AviationContext, command: CommandFunction, error: BadArgument) {
        TODO("Not yet implemented")
    }

    override fun onParseError(ctx: AviationContext, command: CommandFunction, error: Throwable) {
        TODO("Not yet implemented")
    }

    override fun onInternalError(error: Throwable) {
        log.error("Something went wrong internally", error)
    }

    override fun onUnknownCommand(event: MessageReceivedEvent, command: String, args: List<String>) {
        TODO("Not yet implemented")
    }

    override fun onCommandPreInvoke(ctx: AviationContext, command: CommandFunction): Boolean {
        return true
    }

    override fun onCommandPostInvoke(ctx: AviationContext, command: CommandFunction, failed: Boolean) {
        return
    }

    override fun onCommandError(ctx: AviationContext, command: CommandFunction, error: Throwable) {
        if (Milo.aviation.ownerIds.contains(ctx.author.idLong)) {
            val uuid = "${ctx.channel.id}:${ctx.message.id}:${Instant.now()}"
            val msg = ctx.channel
                .sendMessageEmbeds(Embed {
                    title = "Something went wrong"
                    description = "```kotlin\n${error.localizedMessage}\n```"
                })
                .reference(ctx.message)
                .setActionRow(Button.primary("${uuid}/delete", "Delete this"))
                .complete()

            MiloFramework.instance.shardManager.listener<ButtonClickEvent> { event ->
                val id = event.componentId.split("/")
                if (id[0] != uuid) return@listener
                if (event.guild!!.id != ctx.guild.id) return@listener

                event.deferEdit().queue()

                when (id[1]) {
                    "delete" -> {
                        msg
                            .editMessageEmbeds(Embed {
                                title = "Something went wrong"
                                description = "<Message Removed>"
                            })
                            .setActionRows(ActionRow.of())
                            .queue()
                    }
                }
            }
        } else {
            ctx.channel
                .sendMessageEmbeds(Embed {
                    title = "Oh this is unexpected"
                    description = "I failed to run that command, I have sent a message to my developers (don't worry, they can't see what you typed)"
                    footer {
                        name = "This message will self-destruct in 30 seconds"
                    }
                })
                .reference(ctx.message)
                .mentionRepliedUser(false)
                .complete()
                .delete()
                .queueAfter(30, TimeUnit.SECONDS)
        }
        log.error("Error occurred: ${ctx.guild.id}#${ctx.channel.id}:${ctx.message.id} - ${command.name}", error)
    }

    override fun onCommandCooldown(ctx: AviationContext, command: CommandFunction, cooldown: Long) {
        TODO("Not yet implemented")
    }

    override fun onUserMissingPermissions(
        ctx: AviationContext,
        command: CommandFunction,
        permissions: List<Permission>
    ) {
        TODO("Not yet implemented")
    }

    override fun onBotMissingPermissions(
        ctx: AviationContext,
        command: CommandFunction,
        permissions: List<Permission>
    ) {
        TODO("Not yet implemented")
    }
}