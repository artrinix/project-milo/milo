package xyz.artrinix.milo.common.language

import kotlinx.serialization.Serializable

@Serializable
data class LanguageMap(
    val commands: Map<String, Map<String, String>>
    )