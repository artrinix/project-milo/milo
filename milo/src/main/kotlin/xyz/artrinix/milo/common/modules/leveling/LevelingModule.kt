package xyz.artrinix.milo.common.modules.leveling

import dev.minn.jda.ktx.*
import mu.KotlinLogging
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.sharding.ShardManager
import org.jetbrains.exposed.sql.transactions.transaction
import xyz.artrinix.milo.common.database.tables.guild.GuildUser
import xyz.artrinix.milo.common.database.tables.guild.leveling.Leveling
import xyz.artrinix.milo.common.database.types.LevelAnnouncementChannelType
import xyz.artrinix.milo.common.database.types.MessageType
import xyz.artrinix.milo.common.database.types.RoleGiveType
import xyz.artrinix.milo.utils.newSuspendedTransaction
import java.awt.Color
import java.util.*
import kotlin.math.pow

class LevelingModule(shardManager: ShardManager) {
    companion object {
        private val logger = KotlinLogging.logger { }
        private val xpPerLevel = 500

        fun currentLevel(xp: Long): Int {
            var lvl = 0
            while (true) {
                if (xp < requiredXpForLevel(lvl))
                    break
                lvl++
            }
            return lvl - 1
        }

        fun requiredXpForLevel(lvl: Int): Long = ((xpPerLevel / 2 * (lvl.toDouble().pow(2))) + (xpPerLevel / 2 * lvl)).toLong()

        suspend fun leaderboard(): List<GuildUser> {
            return newSuspendedTransaction { GuildUser.all().sortedByDescending { it.xp } }
        }
    }

    private val random = SplittableRandom()

    init {
        shardManager.listener<MessageReceivedEvent> { event ->
            if (!event.isFromGuild) return@listener
            // Return if message is from bot or is a code block
            if (event.message.author.isBot || event.author.isSystem) return@listener
            if (event.message.contentRaw.contains("```")) return@listener

            // Return if the guild is not configured for leveling
            val levelConfig = newSuspendedTransaction { Leveling.findById(event.guild.idLong) } ?: return@listener

            // Find or create a user profile
            var profile = newSuspendedTransaction { GuildUser.findById(event.author.idLong) ?: GuildUser.new(event.author.idLong) {
                xp = 0L
                level = 0
                lastMessageSentAt = 0L
                lastMessageSentHash = 0
                money = 0L
            } }

            // Don't bother calculating if the guild has leveling disabled
            if (!levelConfig.enabled) return@listener

            // Check if the user is talking in a xp disabled channel or contains a xp disabled role
            if (levelConfig.noXpChannels.contains(event.channel.idLong) || (event.member!!.roles.map { it.idLong }.toTypedArray() intersect levelConfig.noXpRoles.toSet()).isNotEmpty()) return@listener

            // Calculate if the message content is over 3 characters long (more than a generic reply)
            // and is not the same as the previous message by the author
            if (event.message.contentStripped.length >= 3 && profile.lastMessageSentHash != event.message.contentStripped.hashCode()) {
                val calculatedMessageSpeed = event.message.contentStripped.lowercase().length.toDouble() / 7
                val diff = System.currentTimeMillis() - profile.lastMessageSentAt

                // Make sure the time between messages is not under the guilds delay, and that the guild has it enabled
                if (levelConfig.experienceDelay != 0 && diff < levelConfig.experienceDelay * 1000) return@listener

                // Checks if the time spent typing the message was long enough to be valid and not a copy-paste script
                if (diff > calculatedMessageSpeed * 1000) {
                    // Removes repeating characters to prevent xp farming by spamming,
                    // aaaaaaah will be valid to gain xp but will only give the same amount as ah, yes it's an exploit
                    val nonRepeatedCharsMessage = event.message.contentStripped.replace(Regex("(.)\\1+"), "$1")

                    // Give a random amount of xp based on the modified message length and the max amount set per guild and multiply it if the guild has a multiplier
                    var gainedXp = levelConfig.experiencePerMessage.coerceAtLeast(
                        random.nextInt(
                            1.coerceAtLeast(nonRepeatedCharsMessage.length / 4),
                            (2.coerceAtLeast(nonRepeatedCharsMessage.length))
                        )
                    ) * 2.0 * levelConfig.experienceMultiplier

                    val newXp = profile.xp + gainedXp.toLong()
                    val newLevel = currentLevel(newXp)

                    if (newLevel > profile.level) {
                        newSuspendedTransaction {
                            val announcement = levelConfig.announcement
                            val channel = when (announcement.channelToSend) {
                                LevelAnnouncementChannelType.SAME_CHANNEL -> event.channel
                                LevelAnnouncementChannelType.DIRECT_MESSAGE -> event.author.openPrivateChannel().complete()
                                LevelAnnouncementChannelType.DIFFERENT_CHANNEL -> event.guild.getTextChannelById(announcement.channelId ?: event.channel.idLong)
                            } ?: return@newSuspendedTransaction

                            val announcementMessage = (announcement.message ?: "Congratulations {DISPLAYNAME} on achieving level {LEVEL}!")
                                .replace("{DISPLAYNAME}", event.member!!.effectiveName)
                                .replace("{LEVEL}", newLevel.toString())


                            val rolesList = levelConfig.roles.filter { it.requiredLevel <= newLevel }.map { it.roles }
                            val roleIds = rolesList.flatMap { it.toSet() }

                            fun sendLevelup() {
                                when (announcement.type) {
                                    MessageType.MESSAGE -> {
                                        channel.sendMessage(announcementMessage).reference(event.message).queue()
                                    }
                                    MessageType.EMBED -> {
                                        channel.sendMessageEmbeds(Embed {
                                            description = announcementMessage
                                            color = Color.GREEN.rgb

                                            if (rolesList.size > 1) {
                                                when (levelConfig.roleGiveType) {
                                                    RoleGiveType.STACK -> {
                                                        field {
                                                            name = "Roles added"
                                                            value = roleIds.map { "<@&$it>" }.joinToString { ", " }
                                                        }
                                                    }
                                                    RoleGiveType.REMOVE -> {
                                                        val rolesBelowCurrentLevel = levelConfig.roles.filter { it.requiredLevel <= newLevel }.sortedByDescending { it.requiredLevel }.toMutableList()
                                                        if (rolesBelowCurrentLevel.isNotEmpty()) {
                                                            val rolesToAdd = rolesBelowCurrentLevel.removeAt(0).roles.map { "<@&$it>" }.joinToString { ", " }
                                                            val rolesToRemove = rolesBelowCurrentLevel.map { it.roles }.flatMap { it.toSet() }.map { "<@&$it>" }.joinToString { ", " }

                                                            field {
                                                                name = "Roles added"
                                                                value = rolesToAdd
                                                            }
                                                            field {
                                                                name = "Roles removed"
                                                                value = rolesToRemove
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }).reference(event.message).queue()
                                    }
                                    MessageType.IMAGE -> TODO("Add level image")
                                }
                            }

                            if (announcement.onlyIfUserReceivedRoles) {
                                if (rolesList.isNotEmpty()) {
                                    sendLevelup()
                                }
                            } else {
                                sendLevelup()
                            }

                            when (levelConfig.roleGiveType) {
                                RoleGiveType.STACK -> {
                                    val rolesResolvable = roleIds.map { event.guild.getRoleById(it) }

                                    if (rolesResolvable.isNotEmpty()) event.guild.modifyMemberRoles(event.member!!, rolesResolvable, listOf()).queue()
                                }
                                RoleGiveType.REMOVE -> {
                                    val rolesBelowCurrentLevel = levelConfig.roles.filter { it.requiredLevel <= newLevel }.sortedByDescending { it.requiredLevel }.toMutableList()

                                    if (rolesBelowCurrentLevel.isNotEmpty()) {
                                        val rolesToAdd = rolesBelowCurrentLevel.removeAt(0).roles.map { event.guild.getRoleById(it) }

                                        val rolesToRemove = rolesBelowCurrentLevel.map { it.roles }.flatMap { it.toSet() }.map { event.guild.getRoleById(it) }

                                        event.guild.modifyMemberRoles(event.member!!, rolesToAdd, rolesToRemove).queue()
                                    }
                                }
                            }
                        }
                    }

                    newSuspendedTransaction {
                        profile.xp = newXp
                        profile.level = newLevel
                        profile.lastMessageSentAt = System.currentTimeMillis()
                        profile.lastMessageSentHash = event.message.contentStripped.hashCode()
                        profile.money = profile.money + levelConfig.moneyPerLevel
                    }
                }
            }

        }
    }
}