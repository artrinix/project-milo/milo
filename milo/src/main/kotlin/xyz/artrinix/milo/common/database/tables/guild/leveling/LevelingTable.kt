package xyz.artrinix.milo.common.database.tables.guild.leveling

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.LongColumnType
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table
import xyz.artrinix.milo.common.database.exposed.array
import xyz.artrinix.milo.common.database.types.RoleGiveType

object LevelingTableToRole : Table("experience_role_to_guild_config") {
    val guild = reference("guild_id", LevelingTable)
    val role = reference("role_id", ExperienceRoleTable)
    override val primaryKey = PrimaryKey(guild, role, name = "PK_LevelingTableToRole_swf_act")
}

object LevelingTable : LongIdTable("leveling") {
    val enabled = bool("enabled").default(false)
    val roleGiveType = enumeration("role_give_type", RoleGiveType::class).default(RoleGiveType.STACK)
    val noXpRoles = array<Long>("no_xp_roles", LongColumnType()).default(arrayOf())
    val noXpChannels = array<Long>("no_xp_channels", LongColumnType()).default(arrayOf())
    val experiencePerMessage = integer("experience_per_message").default(20)
    val experienceDelay = integer("experience_delay").default(30)
    val experienceMultiplier = double("experience_multiplier").default(0.40)
    val moneyPerLevel = long("money_per_level").default(20)
    val announcement = reference("announcement", LevelingAnnouncementTable, onDelete = ReferenceOption.CASCADE)
}

class Leveling(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Leveling>(LevelingTable)

    var enabled by LevelingTable.enabled
    var roleGiveType by LevelingTable.roleGiveType
    var noXpRoles by LevelingTable.noXpRoles
    var noXpChannels by LevelingTable.noXpChannels
    var roles by ExperienceRole via LevelingTableToRole
    var experiencePerMessage by LevelingTable.experiencePerMessage
    var experienceDelay by LevelingTable.experienceDelay
    var experienceMultiplier by LevelingTable.experienceMultiplier
    var moneyPerLevel by LevelingTable.moneyPerLevel
    var announcement by LevelingAnnouncement referencedOn LevelingTable.announcement
}