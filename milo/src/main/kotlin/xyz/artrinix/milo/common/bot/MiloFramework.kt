package xyz.artrinix.milo.common.bot

import dev.minn.jda.ktx.SLF4J
import dev.schlaubi.lavakord.LavaKord
import dev.schlaubi.lavakord.MutableLavaKordOptions
import dev.schlaubi.lavakord.jda.LShardManager
import dev.schlaubi.lavakord.jda.buildWithLavakord
import kotlinx.coroutines.*
import net.dv8tion.jda.api.entities.Activity
import net.dv8tion.jda.api.entities.PrivateChannel
import net.dv8tion.jda.api.exceptions.ErrorResponseException
import net.dv8tion.jda.api.requests.ErrorResponse
import net.dv8tion.jda.api.requests.GatewayIntent
import net.dv8tion.jda.api.requests.RestAction
import net.dv8tion.jda.api.sharding.DefaultShardManagerBuilder
import net.dv8tion.jda.api.sharding.ShardManager
import net.dv8tion.jda.api.utils.ChunkingFilter
import net.dv8tion.jda.api.utils.MemberCachePolicy
import net.dv8tion.jda.api.utils.cache.CacheFlag
import xyz.artrinix.milo.common.bot.sharding.BucketedController
import xyz.artrinix.milo.utils.CoroutineDispatcher
import xyz.artrinix.milo.utils.config.ConfigFramework.Companion.config
import xyz.artrinix.milo.utils.createThreadPool
import java.util.*
import kotlin.coroutines.coroutineContext

class MiloFramework(private val jdaSharder: LShardManager) : LShardManager by jdaSharder {
    private val log by SLF4J

    init {
        log.info("MiloFramework initialized.")
    }

    fun openPrivateChannel(userId: Long): RestAction<PrivateChannel> = shardManager.shards.first { it !== null }.openPrivateChannelById(userId)

    companion object {
        lateinit var instance: MiloFramework

        @OptIn(DelicateCoroutinesApi::class)
        fun createMilo(apply: DefaultShardManagerBuilder.() -> Unit = {}): MiloFramework {
            RestAction.setDefaultFailure(ErrorResponseException.ignore(RestAction.getDefaultFailure(), ErrorResponse.UNKNOWN_MESSAGE))

            instance = DefaultShardManagerBuilder
                .create(
                    config.milo.token,
                    EnumSet.allOf(GatewayIntent::class.java)
                )
                .apply {
                    // General settings
                    setActivityProvider {
                        val activity = config.milo.activity.split(";")
                        Activity.of(Activity.ActivityType.valueOf(activity[0]), activity[1])
                    }

                    // Gateway
                    setSessionController(
                        BucketedController(
                            config.milo.sharding.bucketFactor,
                            config.milo.sharding.homeGuildId
                        )
                    )
                    setShardsTotal(config.milo.sharding.total)
                    if (config.milo.sharding.end > 0)
                        setShards(
                            config.milo.sharding.start,
                            config.milo.sharding.end - 1
                        )
                    setMaxReconnectDelay(32)

                    // Performance
                    setBulkDeleteSplittingEnabled(false)
                    setEnableShutdownHook(true)
                    disableCache(
                        EnumSet.of(
                            CacheFlag.ACTIVITY,
                            CacheFlag.CLIENT_STATUS
                        )
                    )
                    setMemberCachePolicy(MemberCachePolicy.ALL)
                    setChunkingFilter(ChunkingFilter.ALL)
                }
                .apply(apply)
                .buildWithLavakord(newSingleThreadContext("LavaKord Execution Coroutine %d"))
                .let(::MiloFramework)
            return instance
        }
    }
}