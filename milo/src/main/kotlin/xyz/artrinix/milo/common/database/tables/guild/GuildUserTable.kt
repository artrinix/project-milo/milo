package xyz.artrinix.milo.common.database.tables.guild

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable

object GuildUserTable : LongIdTable("guild_user") {
    val xp = long("xp")
    val level = integer("level")
    val lastMessageSentAt = long("last_message_sent_at")
    val lastMessageSentHash = integer("last_message_sent_hash")
    val money = long("money")
}

class GuildUser(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<GuildUser>(GuildUserTable)

    var xp by GuildUserTable.xp
    var level by GuildUserTable.level
    var lastMessageSentAt by GuildUserTable.lastMessageSentAt
    var lastMessageSentHash by GuildUserTable.lastMessageSentHash
    var money by GuildUserTable.money
}