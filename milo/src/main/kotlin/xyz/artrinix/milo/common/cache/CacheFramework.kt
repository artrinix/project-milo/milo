package xyz.artrinix.milo.common.cache

import dev.minn.jda.ktx.SLF4J
import io.lettuce.core.RedisClient
import io.lettuce.core.RedisURI
import io.lettuce.core.api.StatefulRedisConnection
import io.lettuce.core.api.sync.RedisStringCommands
import xyz.artrinix.milo.utils.config.ConfigFramework
import xyz.artrinix.milo.utils.config.ConfigFramework.Companion.config

object CacheFramework {
    private val log by SLF4J

    private val uri = RedisURI.create(config.database.redis.host, config.database.redis.port)
        .let { it.password = config.database.redis.password.toCharArray(); it }

    val client = RedisClient.create(uri)
    val connection: StatefulRedisConnection<String, String> by lazy {
        client.connect()
    }
    val sync: RedisStringCommands<String, String> by lazy {
        connection.sync()
    }

    init {
        log.info("CacheFramework initialized.")
    }
}