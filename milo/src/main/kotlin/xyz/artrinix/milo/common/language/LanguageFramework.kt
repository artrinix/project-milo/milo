package xyz.artrinix.milo.common.language

import com.charleskorn.kaml.Yaml
import dev.minn.jda.ktx.SLF4J
import kotlinx.coroutines.runBlocking
import xyz.artrinix.aviation.i18n.I18nLanguage
import java.io.File
import java.net.URI
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.io.path.nameWithoutExtension
import kotlin.io.path.readText

class LanguageFramework {
    private val log by SLF4J

    init {
        log.info("LanguageFramework initialized.")
    }

    companion object {
        // MutableList of all loaded languages in Aviation format
        val languages: MutableList<I18nLanguage> = mutableListOf()

        /**
         * Helper function for getting a language from the list
         * @param name String
         * @return I18nLanguage?
         */
        fun getLanguage(name: String): I18nLanguage? = languages.find { it.name == name }
    }

    /**
     * Load languages from the classloader resources
     */
    fun loadLanguages() = runBlocking {
        val fs = FileSystems.newFileSystem(URI.create("jar:file:" + this.javaClass.protectionDomain.codeSource.location.toURI().path), mapOf<String, Any>())

        val isJar = LanguageFramework::class.java.getResource("")!!.protocol.endsWith("jar")

        Files.walk(if (isJar) fs.getPath("/lang") else Paths.get(this.javaClass.getResource("/lang")!!.toURI())).filter(Files::isRegularFile).forEach { file ->

            if (!file.fileName.toString().endsWith(".yaml")) return@forEach

            val lang = Yaml.default.decodeFromString(LanguageMap.serializer(), file.readText())
            val table = mutableMapOf<String, String>()

            lang.commands.forEach { (command, keys) ->
                keys.forEach { (key, value) ->
                    table["$command:$key"] = value
                }
            }

            languages.add(I18nLanguage(file.fileName.nameWithoutExtension, table))
        }

        return@runBlocking languages
    }
}