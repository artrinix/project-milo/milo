# Build Milo from the currently pulled source
FROM gradle:7.2.0-jdk16-hotspot as TEMP_BUILD_IMAGE

ENV APP_BUILD=/usr/app/
WORKDIR $APP_BUILD

COPY build.gradle.kts settings.gradle.kts $APP_BUILD
COPY gradle $APP_BUILD/gradle
COPY --chown=gradle:gradle . /home/gradle/src
USER root
RUN chown -R gradle /home/gradle/src
COPY . .
RUN export MILO_VERSION=$(git rev-parse --short HEAD)
RUN gradle clean shadowJar

# Run the built Milo Jar from the image
FROM openjdk:16-jdk-alpine

ENV ARTIFACT_NAME=milo.jar
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME

# Copy the built jar from the build image
COPY --from=TEMP_BUILD_IMAGE $APP_HOME/milo/build/libs/$ARTIFACT_NAME .
# Expose the required API port
EXPOSE 8080
ENTRYPOINT exec java -jar ${ARTIFACT_NAME}
